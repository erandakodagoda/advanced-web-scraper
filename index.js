
const PORT = 3001
const express = require('express')
const axios = require('axios')
const cheerio = require('cheerio')
const { response } = require('express')

const app = express()


axios('https://www.forbes.com')
    .then(response => {
        const html = response.data;
        const $ = cheerio.load(html);
        const feeds = []

        $('.headlink', html).each(function() {
            const title = $(this).text();
            const url = $(this).attr('href');
            feeds.push({
                title,
                url
            })
        })
        console.log(feeds);

    }).catch(err => console.log(err))

app.listen(PORT, () => console.log(`Scraper running on port: ${PORT}`))